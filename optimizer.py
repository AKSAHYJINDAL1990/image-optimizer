from PIL import Image
import os
import PIL
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import sys

def messgae_popup(messsage):
    messagebox.showinfo("Optimizer", messsage)


def optimize(source, dest):
    try:
        path = os.path.realpath(dest.get())
        source_path = os.path.realpath(source.get())
        unopt_images = [file for file in os.listdir(source_path) if file.endswith(('jpg', 'jpeg', 'png'))]
        for image in unopt_images:
            img = PIL.Image.open(os.path.join(source_path, image))
            img.save(os.path.join(path, image), optimize=True, quality=15)
        messgae_popup("sucessfully optimized")
    except:
        e = sys.exc_info()[0]
        messgae_popup("Failed to optimize images" + e)

top = Tk()
def open_directory(textbox):
    address = filedialog.askdirectory()
    textbox.insert(INSERT, address)



#top.geometry('200x200')

sourcePathString = StringVar()
sourcePathLabel = Label(top, textvariable=sourcePathString)
sourcePathString.set("Image optimizer Source:")
sourcePathLabel.pack()

sourceValue = StringVar()
sourceTextBox = Entry(top, textvariable=sourceValue, width=30)
sourceTextBox.pack()

sourceDirButton = Button(top, text="Browse...", command=lambda : open_directory(sourceTextBox))
sourceDirButton.pack()

destPathString = StringVar()
destPathLabel = Label(top, textvariable=destPathString)
destPathString.set("Image optimizer destination:")
destPathLabel.pack()

destValue = StringVar()
destTextBox = Entry(top, textvariable=destValue, width=30)
destTextBox.pack()

destDirButton = Button(top, text="Browse...", command=lambda : open_directory(destTextBox))
destDirButton.pack()

B = Button(top, text='Optimize', command= lambda: optimize(sourceTextBox, destTextBox))
B.pack()

top.mainloop()

